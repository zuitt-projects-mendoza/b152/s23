db.users.insertMany([
{
    "firstName": "Billie Joe",
    "lastName": "Armstrong",
    "email": "notTheVocalist@gmail.com",
    "password": "greenday1972",
    "isAdmin": false   
    
},
{
    "firstName": "Mike",
    "lastName": "Dirnt",
    "email": "notTheBassist@gmail.com",
    "password": "pritchard4",
    "isAdmin": false   
    
},
{
    "firstName": "Tre",
    "lastName": "Cool",
    "email": "notTheDrummer@gmail.com",
    "password": "wrightthe3rd",
    "isAdmin": false   
    
}
]);

db.courses.insertMany([
{
    "name": "Math 101",
    "price": "Learn the basics of Math",
    "isActive": false   
    
},
{
    "name": "English 101",
    "price": "English as a Second Language",
    "isActive": false   
    
},
{
    "name": "Science 101",
    "price": "The Human Body",
    "isActive": false   
    
}
]);

db.users.find({"isAdmin": false}) ;
db.courses.find({"isActive": false});

db.users.updateOne({},{$set:{"isAdmin": true}});
db.courses.updateOne({},{$set:{"isActive": true}});

db.courses.deleteMany({"isActive": false});